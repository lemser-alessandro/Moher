#!/usr/bin/env python3

from queue import Queue
from threading import Thread
import datetime
import time
from dateutil.relativedelta import relativedelta
import flickrapi
from flask import Flask, render_template, redirect, url_for, request, session
app = Flask(__name__)

api_key = u'820a5f399dc0032c41be29241cecdf36'
api_secret = u'33d128d07ba9406a'
app.secret_key = "campeche"
relevant_exif = ['DateTimeOriginal', 'FNumber', 'ISO', 'FocalLength', 'ExposureTime', 'FocalLengthIn35mmFormat']
exif_csv_list = []
gen_queue = Queue(maxsize=1)
ids_queue = Queue(maxsize=1)

@app.route("/home")
def index():
    flickr = flickrapi.FlickrAPI(api_key, api_secret)
    if flickr.token_valid(perms='read'):
        return "<a href='create_csv'>Create CSV</a>"
    else:
        return "<a href='login'>Login</a>"

@app.route('/login')
def login():
    flickr = flickrapi.FlickrAPI(api_key, api_secret)
    if flickr.token_valid(perms='read'):
        print('Already logged in, redirecting to index.')
        return redirect(url_for('/home'))

    # Get the request token
    callback = url_for('auth_ok', _external=True)
    print('Getting request token with callback URL %s' % callback)
    flickr.get_request_token(oauth_callback=callback)

    authorize_url = flickr.auth_url(perms='read')

    # Store it in the session, to use in auth_ok()
    session['request_token'] = flickr.flickr_oauth.resource_owner_key
    session['request_token_secret'] = flickr.flickr_oauth.resource_owner_secret
    session['requested_permissions'] = flickr.flickr_oauth.requested_permissions

    print('Redirecting to %s.' % authorize_url)
    return redirect(authorize_url)
    
@app.route('/auth_ok')
def auth_ok():
    flickr = flickrapi.FlickrAPI(api_key, api_secret)
    flickr.flickr_oauth.resource_owner_key = session['request_token']
    flickr.flickr_oauth.resource_owner_secret = session['request_token_secret']
    flickr.flickr_oauth.requested_permissions = session['requested_permissions']
    verifier = request.args['oauth_verifier']

    print('Getting resource key')
    flickr.get_access_token(verifier)
    return redirect('/home')

@app.route("/create_csv")
def create_csv():
    print("Process exif")
    flickr = flickrapi.FlickrAPI(api_key, api_secret)
    
    now = datetime.datetime.now()
    time_before = now - relativedelta(days=30)
    while time_before.year >= 2004:
        generator = flickr.walk(user_id='_a_a', min_taken_date=time_before.strftime('%Y-%m-%d'), max_taken_date=str(now.strftime('%Y-%m-%d')), per_page=100)
        ids = [p.get('id') for p in generator]
        print("Got ids")
        ex = None
        for photoId in ids:
            try:
                extract_exif(photoId)
            except Exception as e:
                print(e)
                ex = "Photo ", photoId, "has failed"

        if ex:
            print(ex)
        else:
            print("Processing between ", time_before, now)
            now = time_before + relativedelta(days=1)
            time_before = now - relativedelta(days=30)

    return redirect('/home')

def extract_exif(photoId):
    focal_len = None
    focal_len_35eq = None
    iso = None
    dt = None
    fnum = None
    exp = None

    flickr = flickrapi.FlickrAPI(api_key, api_secret)
    print("Processing photo " + photoId)
    jsonexif = flickr.photos_getExif(photo_id=photoId, format='parsed-json')
    camera = '"'+jsonexif['photo']['camera']+'"'

    if 'samsung SM-A310F' in camera:
        return

    for row in jsonexif['photo']['exif']:
        if iso and dt and focal_len and focal_len_35eq:
            break

        exif_val = row['raw']['_content'] or '0'
        if row['tag'] == 'FocalLength':
            focal_len = exif_val.replace("mm", '')
        if row['tag'] == 'FocalLengthIn35mmFormat':
            focal_len_35eq = exif_val.replace("mm", '')
        if row['tag'] == 'ISO':
            iso = exif_val
        if row['tag'] == 'FNumber':
            fnum = exif_val
        if row['tag'] == 'ExposureTime':
            exp = '"'+exif_val+'"'
        if row['tag'] == 'DateTimeOriginal':
            dt = '"'+exif_val+'"'

    fl = focal_len_35eq or focal_len
    if fl:
        f = open('exif.csv', "a")
        f.write(camera + ', ' + dt + ', ' + iso+ ', ' + fnum + ', ' + exp + ', ' + fl)
        f.write('\n')
        f.close()

if __name__ == "__main__":
    csv_file = open('exif.csv', "w")
    csv_file.close()
    
    app.debug = True
    app.run(host='0.0.0.0', port=8080)