#!/usr/bin/env python3

import flickrapi
from flask import url_for, request, session

class MoherFlickr:

    api_key = u'820a5f399dc0032c41be29241cecdf36'
    api_secret = u'33d128d07ba9406a'
    app.secret_key = "campeche"

    def login(session):
        flickr = flickrapi.FlickrAPI(api_key, api_secret)
        if flickr.token_valid(perms='read'):
            print('Already logged in')
            return None

        # Get the request token
        callback = url_for('auth_ok', _external=True)
        print('Getting request token with callback URL %s' % callback)
        flickr.get_request_token(oauth_callback=callback)
    
        authorize_url = flickr.auth_url(perms='read')
    
        # Store it in the session, to use in auth_ok()
        session['request_token'] = flickr.flickr_oauth.resource_owner_key
        session['request_token_secret'] = flickr.flickr_oauth.resource_owner_secret
        session['requested_permissions'] = flickr.flickr_oauth.requested_permissions
    
        print('Redirecting to %s.' % authorize_url)
        return authorize_url
        
    def register():
        flickr = flickrapi.FlickrAPI(api_key, api_secret)
        flickr.flickr_oauth.resource_owner_key = session['request_token']
        flickr.flickr_oauth.resource_owner_secret = session['request_token_secret']
        flickr.flickr_oauth.requested_permissions = session['requested_permissions']
        verifier = request.args['oauth_verifier']
    
        print('Getting resource key')
        flickr.get_access_token(verifier)